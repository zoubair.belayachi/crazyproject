import React, { useState, useEffect } from 'react';
import { Image, ImageBackground, Pressable, StyleSheet, Text, View } from 'react-native';
import lettre from '../services/LettresService';
import themes from '../services/ThemesService';
import BackGroundThemeAleatoire from '../services/BackGroundThemeAleatoire';
import BackGroundLettreAleatoire from '../services/BackGroundLettreAleatoire';

const RandomComponent = ({ navigation }) => {
  const [backgroundColor, setBackgroundColor] = useState(null);
  const [backGroundTheme, setBackGroundTheme] = useState(null);
  const [backGroundLettre, setBackGroundLettre] = useState(null);
  
  const chiffreL = Math.floor(Math.random() * 24);
  const chiffreT = Math.floor(Math.random() * 14);
  const themeNom = themes[chiffreT]["theme"]
  const themeImage = themes[chiffreT]["image"]
  const url  = lettre[chiffreL]

  const colors = ['#18D5FF', '#FF0000', '#FFE602'];

  useEffect(() => {
    const randomColor = colors[Math.floor(Math.random() * colors.length)];
    setBackgroundColor(randomColor);
  }, []);

  useEffect(() => {
    if (backgroundColor) {
      setBackGroundTheme(BackGroundThemeAleatoire[backgroundColor]);
    }
  }, [backgroundColor]);

  
  useEffect(() => {
    if (backgroundColor) {
      setBackGroundLettre(BackGroundLettreAleatoire[backgroundColor]);
    }
  }, [backgroundColor]);

  // console.log(backGroundTheme);

  return (
    <View style={[styles.divContainer, { backgroundColor }]}>
      <View style={styles.container}>
        <Text style={[styles.themeNom]}>{themeNom}</Text>
      </View>

      <View style={[styles.container, styles.imageAleatoireContainer]}>
        <ImageBackground source={backGroundTheme} style={styles.BackGroundImageThemeAleatoire}>
          <Image source={themeImage} style={styles.imageThemeAleatoire}/>
        </ImageBackground>
      </View>

      <View style={styles.lettreAleatoire}>
        <View style={styles.lettre}>
          <ImageBackground source={backGroundLettre} style={styles.lettreBackGround}>
            <Image source={url}/>
          </ImageBackground>
        </View>

        <View>
          <Pressable style={styles.button} onPress={() => navigation.push('RandomComponent')}>
            <Image style={{width: 80, height:80}} source={require('../assets/images/fonds/nextbutton.png')} />
          </Pressable>
        </View>
      </View>

      <View style={[styles.container, styles.containerGage]}>
        <Pressable style={styles.button} onPress={() => {}}>
          <Text style={styles.text}>5 points gagnés =</Text>
          <Text style={styles.text}>tu donnes un gage à qui tu veux !</Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  divContainer: {
    flex: 1,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageAleatoireContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  BackGroundImageThemeAleatoire: {
    justifyContent: 'center',
    alignItems: 'center',
    // height: '95%',
    // width: '95%',
    resizeMode: 'cover',
    paddingHorizontal: '14%'
  },
  imageThemeAleatoire: {
    height: 300,
    width: 250,
  },
  lettreAleatoire: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingBottom: '4%',
  },
  containerGage: {
    backgroundColor: 'black',
    justifyContent: 'flex-end',
    paddingVertical: '1%',
  },
  text: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  lettre: {
    // padding: '5%',
    // paddingTop: '15%',
    // paddingLeft: '5%',
  },
  lettreBackGround: {
    padding: '10%',
    // paddingTop: '15%',
    // paddingLeft: '5%',
  },
  lettreImage: {
    paddingTop: '15%',
    paddingBottom: '15%',
  },
  button:{
    alignItems: 'center',
    justifyContent: 'center',
  },
  themeNom: {
    paddingTop: "30%",
    textAlign: "center",
    fontFamily: "Stora",
    fontSize: 50,
    color: '#FFFFFF',
  }
});

export default RandomComponent;