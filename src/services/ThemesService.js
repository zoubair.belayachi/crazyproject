const themes = [
    {theme:"Chambre", image: require("../assets/images/themes/Frame-1.png"), parler:"Dans une chambre"},
    {theme:"Salon abandonné", image: require("../assets/images/themes/Frame-2.png"), parler:"Dans la maison"},
    {theme:"Lac", image: require("../assets/images/themes/Frame-3.png"), parler:"A la mer"},
    {theme:"Vêtement", image: require("../assets/images/themes/Frame-4.png"), parler:"Un Vêtement"},
    {theme:"Nuit", image: require("../assets/images/themes/Frame.png"), parler:"Dans la nuit"},
    {theme:"Qui se boit", image: require("../assets/images/themes/alcool.png"), parler:"Quelque chose qui se boit"},
    {theme:"Animal", image: require("../assets/images/themes/chat.png"), parler:"Un animal"},
    {theme:"Acteur/trice", image: require("../assets/images/themes/cinema.png"), parler:"Un acteur ou une actrice"},
    {theme:"Cirque", image: require("../assets/images/themes/cirque.png"), parler:"Au cirque"},
    {theme:"Cuisine", image: require("../assets/images/themes/cuisine.png"), parler:"Dans la cuisine"},
    {theme:"Fruit", image: require("../assets/images/themes/fruit.png"), parler:"Un fruit"},
    {theme:"Chanteur/euse", image: require("../assets/images/themes/music.png"), parler:"Un chanteur ou une chanteuse"},
    {theme:"Pays", image: require("../assets/images/themes/planete.png"), parler:"Un pays"},
    {theme:"Sport", image: require("../assets/images/themes/sport.png"), parler:"Un sport"},
    {theme:"Ville", image: require("../assets/images/themes/ville.png"), parler:"Une ville"},
]

export default themes;