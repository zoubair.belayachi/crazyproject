import React from 'react';
import { StyleSheet, Text, View, Button, Image, ImageBackground,Pressable, TextInput, TouchableHighlight } from 'react-native';

const Accueil = ({ navigation }) => {


  const onPressJouer = () => {
    navigation.navigate('Regles');
  };


  return (
    <>
      <View style={styles.divAll}>        
        <View>
          <ImageBackground
            resizeMode='stretch'
            style={styles.ImageHautJaune}
            source={require('../assets/images/fonds/jaunehautdroit.png')}
          />
        </View>
        <View style={styles.container}>
          <ImageBackground
            source={require('../assets/images/fonds/Fond1.png')}
            resizeMode='cover'
            style={styles.imageBackground}
          >
            <Text style={styles.buttonText} onPress={onPressJouer}>
              Jouer
            </Text>
          </ImageBackground>
        </View>
        <View>
          <ImageBackground source={require('../assets/images/fonds/jauneenbas.png')}>
            <Image style={styles.perso} source={require('../assets/images/fonds/personnageenbasagauche.png')} />
          </ImageBackground>
        </View>
       
    </View>

    </>
  );
};

const styles = StyleSheet.create({
  input: {
    alignSelf: 'stretch',
    height: 20,
    borderBottomWidth: 2,
    borderBottomColor: "red",
    margin: 8
  },
  divAll : {
    height: "100%",
    width: "100%",
  },
  container: {
    marginHorizontal: "7%",
    justifyContent: 'center',
    paddingBottom: "4%",
    paddingTop: "57%",
  },
  imageBackground: {
    paddingVertical: "3%",
  },
  buttonText: {
    fontFamily: 'Stora',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 70,
  },
  ImageHautJaune: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: "36%",
  },
  perso: {
    marginTop: "19%",
  },
});

export default Accueil;