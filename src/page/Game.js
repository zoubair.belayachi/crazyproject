import React from "react";
import RandomComponent from "../component/RandomComponent";

const Game = ({ navigation }) => {
  return (
    <RandomComponent navigation={navigation} />
  );
};

export default Game;