import { FlatList, StyleSheet, Text, View, Button, ImageBackground, Image, Pressable,Dimensions } from 'react-native';
import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';

const { width } = Dimensions.get('window');

const data = [
  { id: 0, title: require("../assets/images/tuto/tuto1.jpg") },
  { id: 1, title: require("../assets/images/tuto/tuto2.jpg") },
  { id: 2, title: require("../assets/images/tuto/tuto3.jpg") },
  { id: 3, title: require("../assets/images/tuto/tuto4.jpg") },
  { id: 4, title: require("../assets/images/tuto/tuto5.jpg") },
  { id: 5, title: require("../assets/images/tuto/tuto6.jpg") },
  { id: 6, title: require("../assets/images/tuto/tuto7.jpg") },
  { id: 7, title: require("../assets/images/tuto/tuto8.jpg") },
  { id: 8, title: require("../assets/images/tuto/tuto9.jpg") },
  { id: 9, title: require("../assets/images/tuto/tuto10.jpg") },
  { id: 10, title: require("../assets/images/tuto/tuto11.jpg") },
];

const Tuto = ({}) => {
const [activeIndex, setActiveIndex] = useState(0);
const navigation = useNavigation();

const Jouer = (item) => {
    navigation.navigate('Game');
};

const Prenium = (item) => {
    navigation.navigate('Prenium');
};

const renderItem = ({ item, navigation }) => (
    <View style={styles.page}>
      {item.id === 10 ? (
        <Pressable onPress={Jouer} style={styles.tailleImagePressable}>
          <Image style={styles.tailleImage} source={item.title} />
        </Pressable>
      ) : item.id === 9 ? (
        <Pressable onPress={Prenium} style={styles.tailleImagePressable}>
          <Image style={styles.tailleImage} source={item.title} />
        </Pressable>
      ) : (
        <Image style={styles.tailleImage} source={item.title} />
      )}
    </View>
);

  const handlePageChange = (event) => {
    const offset = event.nativeEvent.contentOffset.x;
    const index = Math.round(offset / width);
    setActiveIndex(index);
    // console.log(index);
  };

  return (
    <>
   <View style={styles.container}>
      <FlatList
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        onScroll={handlePageChange}
      />
      <View style={styles.pagination}>
        {data.map((item, index) => (
          <View
            key={item.id}
            style={[
              styles.paginationDot,
              activeIndex === index && styles.activePaginationDot,
            ]}
          />

        ))}
      </View>
    </View>

    </>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
      },
      page: {
        width,
        justifyContent: 'center',
        alignItems: 'center',
      },
      pagination: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 16,
        left: 0,
        right: 0,
      },
      paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: 'gray',
        marginHorizontal: 4,
      },
      activePaginationDot: {
        backgroundColor: 'black',
      },
      tailleImage: {
        height: "78%",
        width: "95%"
      },
      tailleImagePressable: {
        height: "100%",
        width: "100%",
        marginTop: "50%",
        alignItems: "center",
      },
});

export default Tuto;