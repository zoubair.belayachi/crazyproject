import { FlatList, StyleSheet, Text, View, Button, ImageBackground, Image, Pressable,Dimensions } from 'react-native';
import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';

const { width } = Dimensions.get('window');


const Prenium = ({}) => {
const navigation = useNavigation();


  return (
    <>
    <View style={styles.containerParent}>
        <View style={styles.container}>
          <Text style={[styles.text,styles.textGras]}>VERSION PRENIUM</Text>
          <Text style={[styles.text]}>La version gratuite, c’est 15 thèmes, 5 lettres et 3 gages… bon début.</Text>
          <Text style={[styles.text,styles.textGras]}>Tu veux TOUT ? pour 1,99 €</Text>
          <Text style={[styles.text]}>70 thèmes, 15 lettres et les 5 gages débloqués ! Télécharge la VERSION PREMIUM 
            pour 1,99€ !
          </Text>

          <Pressable
            style={[styles.playstore]}
            >
            <Text style={styles.text}>OUI, JE VEUX TOUT</Text>
          </Pressable>


        </View>
    </View>
    </>
  );
};

const styles = StyleSheet.create({
    containerParent: {
        flex: 1,
        justifyContent: 'center',
        // alignContent: 'center',
        alignItems: 'center'
      },
    container: {
        width: "80%",
        height: "83%",
        borderRadius: 30,
        borderColor: "black",
        // backgroundColor: "red",
        borderWidth: 8,
        marginTop: "20%",
        // textAlign: "center",
    },
    text: {
      textAlign: "center",
      // fontStyle: bold,
      fontSize: 23,
      paddingTop: "5%",
      paddingBottom: "5%",
      paddingHorizontal: "5%"
    },
    textGras: {
      // fontWeight: 700,
      fontSize: 30
    },
    playstore: {
      backgroundColor: "#e355d9",
      marginHorizontal: "5%",
      borderRadius: 10,
    }
});

export default Prenium;