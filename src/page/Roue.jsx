import WheelOfFortune from 'react-native-wheel-of-fortune'
import {useState} from 'react';
import { Button , View, StyleSheet, Alert, Image,Text, ImageBackground,Modal, Pressable} from 'react-native';
import veriter from '../assets/images/Roue/veriter.png';
import echange from '../assets/images/Roue/echange.png';
import interro from '../assets/images/Roue/interrogation.png';
import cadenaun from '../assets/images/Roue/cadenas.png';
import cadena from '../assets/images/Roue/cadena.png';
import bgroue from '../assets/images/Roue/bgroue.png';
import { useNavigation } from '@react-navigation/native';

const Roue = () => {

    const [modalVisible, setModalVisible] = useState(false);
    state = {modalVisible: false,};

    const [resultat, setResultat] = useState("");

    const navigation = useNavigation();

    const move = () => {
        navigation.navigate("Game");
    }

    const participants = [
        {image : veriter, left: 110, top: -25, text: 'Action'},
        {image : echange, left: -70 , top: -320, text: 'Echange de joueur qui doit tourner la roue'},
        {image : interro, left: -320, top: -140, text: 'Veriter'},
        {image : cadenaun, left: 320, top: 110, text: 'Version Premium'},
        {image : cadena, left: 240, top: -130, text: 'Version Premium'},
    ];

    const wheelOptions = {
        rewards: participants,
        knobSize: 25,
        borderWidth: 10,
        borderColor: '#263238',
        innerRadius: 50,
        duration: 4000,
        backgroundColor: 'transparent',
        textAngle: 'horizontal',
        knobSource: require('../assets/images/Roue/newKnob.png'),
        getWinner: (value, index) => {
            // alert(value.text);
            
            setModalVisible(true);
            setResultat(value.text);

        },
        onRef: ref => (this.child = ref),
    };

    console.log(modalVisible);

    return (
        <View style={{height: "100%"}}>
            <ImageBackground source={bgroue}>
                    <View>
                    {/* <Text>efezfzefezfz</Text> */}
                </View>
                <View style={styles.roue}>
                    <WheelOfFortune  wheelOptions={wheelOptions} options={wheelOptions} />
                </View>
                <View>
                    <Text></Text>
                </View>
                <View style={styles.btnRoue}>
                    <Pressable style={styles.styleBtn}  onPress={() => { this.child._tryAgain() }}>
                        <Text style={styles.textStyle}> ? ? ?</Text>
                    </Pressable>
                </View>

                <View style={styles.retourAuJeu} >
                    <Text style={styles.textretourAuJeu} onPress={() =>move()}>
                    Retour au jeu
                    </Text>
                </View>

                <View style={styles.centeredView}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            setModalVisible(false);
                        }}>
                        <View style={styles.centeredView}>
                            
                            <View style={styles.modalView}>
                                <Pressable
                                style={[styles.buttonCroix]}
                                onPress={() => setModalVisible(false)}>
                                </Pressable>
                            
                                <Text onPress={() => setModalVisible(false)} style={[styles.textStyle, styles.modalGage]}>{resultat}</Text>

                            </View>
                        </View>
                    </Modal>
                </View>

            </ImageBackground>
        </View>
    )
};
const styles = StyleSheet.create({
    roue : {
        marginTop : "90%"
    },
    btnRoue : {
        marginTop : "58%",
        // paddingBottom: "24%",
        marginHorizontal: "20%",
    },
    styleBtn: {
        paddingVertical: "10%",
        backgroundColor: "black",
        color: "white",
        borderWidth: 6,
        borderColor: "white",
        borderRadius: 30,
    },
    centeredView: {
        marginTop: "5%",
        justifyContent: 'center',
    },
    modalView: {
        marginTop: "50%",
    },
    textStyle: {
        color: "white",
        fontFamily: "Stora",
        fontSize: 45,
        textAlign: "center",
    },
    modalGage: {
        paddingVertical: "10%",
        backgroundColor: "black",
        color: "white",
        borderWidth: 6,
        borderColor: "white",
        borderRadius: 30,
        marginTop : "90%",
        paddingBottom: "4%",
        marginHorizontal: "20%",
        fontSize: 25,
    },
    retourAuJeu: {
        // marginTop: "1%"
        backgroundColor: "black",
        marginTop: "10%",
    },
    textretourAuJeu: {
        textAlign: "center",
        color: "white",
        fontSize: 25,
       fontStyle: "bold",
       paddingVertical: "2%"
    }
});

export default Roue;