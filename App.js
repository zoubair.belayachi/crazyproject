import {StyleSheet, Image,Pressable,View,Modal,Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Accueil from "./src/page/Accueil";
import Regles from "./src/page/Regles";
import {useFonts} from "expo-font";
import Game from "./src/page/Game";
import LogoParametre from "./src/assets/images/fonds/Parametres.png";
// import LogoVolume from "./src/assets/images/fonds/volume.png"
import LogoRoue from "./src/assets/images/fonds/newRoue.png"
import { useState } from 'react';
import RandomComponent from "./src/component/RandomComponent";
import ButtonParams from "./src/component/ButtonParams";
import ButtonVolume from "./src/component/ButtonVolume";
import ButtonRoue from "./src/component/ButtonRoue";
import Tuto from "./src/page/Tuto";
import Prenium from "./src/page/Prenium"
import Roue from "./src/page/Roue";

export default function App({}) {

    const [fontsLoaded] = useFonts({
        'Stora': require('./src/assets/fonts/stora/Stora.ttf'),
        'Poppins': require('./src/assets/fonts/poppins/Poppins-Regular.ttf'),
        'Poppins-bold': require('./src/assets/fonts/poppins/Poppins-Bold.ttf'),
    });
    if (!fontsLoaded) {
        return null;
    }
    

    const Stack = createNativeStackNavigator();
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={({navigation}) => ({
           headerTransparent: true,
           headerTitle: () => <ButtonVolume/> ,
           headerTitleAlign: 'center',
            headerRight: () => <ButtonRoue image={LogoRoue}/>,
            headerLeft: () => <ButtonParams image={LogoParametre}/>,
            headerBackVisible: false
        })} 
           initialRouteName="Accueil">
                <Stack.Screen name="Accueil" component={Accueil}/>
                <Stack.Screen name="Regles" component={Regles}/>
                <Stack.Screen name="Game" component={Game}/>
                <Stack.Screen name="RandomComponent" component={RandomComponent}/>
                <Stack.Screen name="Tuto" component={Tuto}/>
                <Stack.Screen name="Prenium" component={Prenium}/>
                {/* <Stack.Screen name="Roue" component={Roue} options={{headerLeft:() => {
                }}}/> */}
                <Stack.Screen name="Roue" component={Roue}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
};